Recent Posts
============

2020
----
.. toctree::
    :maxdepth: 1
    :glob:
    
    posts/2020/*

2019
----

.. toctree::
    :maxdepth: 1
    :glob:
    
    posts/2019/*


2018
----

.. toctree::
    :maxdepth: 1
    :glob:
    
    posts/2018/*
